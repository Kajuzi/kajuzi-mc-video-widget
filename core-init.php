<?php 
/*
*
*	***** Kajuzi MC Video Widget *****
*
*	This file initializes all KMVW Core components
*	
*/
// If this file is called directly, abort. //
if ( ! defined( 'WPINC' ) ) {die;} // end if
// Define Our Constants
define('KMVW_CORE_INC',dirname( __FILE__ ).'/assets/inc/');
define('KMVW_CORE_IMG',plugins_url( 'assets/img/', __FILE__ ));
define('KMVW_CORE_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('KMVW_CORE_JS',plugins_url( 'assets/js/', __FILE__ ));
/*
*
*  Register CSS
*
*/
function kmvw_register_core_css(){
wp_enqueue_style('kmvw-core', KMVW_CORE_CSS . 'kmvw-core.css',null,time('s'),'all');
};
add_action( 'wp_enqueue_scripts', 'kmvw_register_core_css' );    
/*
*
*  Register JS/Jquery Ready
*
*/
function kmvw_register_core_js(){
// Register Core Plugin JS	
wp_enqueue_script('kmvw-core', KMVW_CORE_JS . 'kmvw-core.js','jquery',time(),true);
};
add_action( 'wp_enqueue_scripts', 'kmvw_register_core_js' );    
/*
*
*  Includes
*
*/ 
// Load the Functions
if ( file_exists( KMVW_CORE_INC . 'kmvw-core-functions.php' ) ) {
	require_once KMVW_CORE_INC . 'kmvw-core-functions.php';
}     
// Load the ajax Request
if ( file_exists( KMVW_CORE_INC . 'kmvw-ajax-request.php' ) ) {
	require_once KMVW_CORE_INC . 'kmvw-ajax-request.php';
} 
// Load the Shortcodes
if ( file_exists( KMVW_CORE_INC . 'kmvw-shortcodes.php' ) ) {
	require_once KMVW_CORE_INC . 'kmvw-shortcodes.php';
}