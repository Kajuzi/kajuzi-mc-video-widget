<?php

class Kmvw_Gallery_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
			'kmvw_gallery_widget',
			__( 'KMVW Video Gallery', 'text_domain' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}

	// The widget form (for the backend )
	public function form( $instance ) {

		// Set widget defaults
		$defaults = array(
			'title'    => '',
			'subheading'     => '',
			'posts_per_page' => '',
			'layout'   => '',
		);
		
		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php // Text Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subheading' ) ); ?>">Subheading</label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subheading' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subheading' ) ); ?>" type="text" value="<?php echo esc_attr( $subheading ); ?>" />
		</p>

		<?php // Dropdown ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ); ?>">Style</label>
			<select name="<?php echo $this->get_field_name( 'layout' ); ?>" id="<?php echo $this->get_field_id( 'layout' ); ?>" class="widefat">
			<?php
			// Your options array
			$options = array(
				'1+3'       => '1 large & 3 thirds',
				'grid' 		=> 'Grid',
			);

			// Loop through options and add each one to the layout dropdown
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $layout, $key, false ) . '>'. $name . '</option>';

			} ?>
			</select>
		</p>

		<?php // Dropdown ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>">Posts To Display</label>
			<select name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" class="widefat">
			<?php
			// Your options array
			$options = array(
				1       => '1',
				3 		=> '3',
				4 		=> '4',
				6 		=> '6',
				7 		=> '7',
				0 		=> 'All',
			);

			// Loop through options and add each one to the posts_per_page dropdown
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $posts_per_page, $key, false ) . '>'. $name . '</option>';

			} ?>
			</select>
		</p>

	<?php }

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['subheading']     = isset( $new_instance['subheading'] ) ? wp_strip_all_tags( $new_instance['subheading'] ) : '';
		$instance['posts_per_page']   = isset( $new_instance['posts_per_page'] ) ? wp_strip_all_tags( $new_instance['posts_per_page'] ) : '';
		$instance['layout']   = isset( $new_instance['layout'] ) ? wp_strip_all_tags( $new_instance['layout'] ) : '';
		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {

		extract( $args );

		// Check the widget options
		$title    		= isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$subheading 	= isset( $instance['subheading'] ) ? $instance['subheading'] : '';
		$posts_per_page = isset( $instance['posts_per_page'] ) ? $instance['posts_per_page'] : '';
		$layout   		= isset( $instance['layout'] ) ? $instance['layout'] : '';

		// WordPress core before_widget hook (always include )
		echo $before_widget;

		// Display the widget
		echo '<div class="widget-text wp_widget_plugin_box">';


		?>
		<div class="container-fluid pb-video-container">
		    <div class="col-md-10 offset-md-1">
		<?php
				// Display widget title if defined
				if ( $title ) {
					echo $before_title . $title . $after_title;
				}
				// Display subheading field
				if ( $subheading ) {
					echo '<p>' . $subheading . '</p>';
				}

				$videos = new WP_Query( array( 'post_type' => 'video', 'meta_key'  => 'youtube_url', ) );

		?>
		        <div class="row pb-row">
		<?php foreach ( $videos->posts as $vid ): ?>
		            <div class="col-md-4 pb-video">
		                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/<?php echo get_post_meta($vid->ID, 'youtube_id', true) ?>" frameborder="0" allowfullscreen></iframe>
		                <label class="form-control label-warning text-xs-center"><?php echo $vid->post_title ?></label>
		            </div>
		<?php endforeach; ?>
		        </div>
		    </div>
		</div>

		<style>
		    .pb-video-container {
		        padding-top: 20px;
		    }

		    .pb-video {
		        border: 1px solid #e6e6e6;
		        padding: 5px;
		    }

		        .pb-video:hover {
		            background: #2c3e50;
		        }

		    .pb-video-frame {
		        transition: width 2s, height 2s;
		    }

		        .pb-video-frame:hover {
		            height: 300px;
		        }

		    .pb-row {
		        margin-bottom: 10px;
		    }
		</style>
		<?php

		echo '</div>';

		// WordPress core after_widget hook (always include )
		echo $after_widget;

	}

}

// Register the widget
function kmvw_register_gallery_widget() {
	register_widget( 'Kmvw_Gallery_Widget' );
}
add_action( 'widgets_init', 'kmvw_register_gallery_widget' );