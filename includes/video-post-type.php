<?php
/**
 * Register a custom post type called "video".
 *
 * @see get_post_type_labels() for label keys.
 */
function kmvw_video_init() {
    $labels = array(
        'name'                  => _x( 'Videos', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Video', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Videos', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Video', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Video', 'textdomain' ),
        'new_item'              => __( 'New Video', 'textdomain' ),
        'edit_item'             => __( 'Edit Video', 'textdomain' ),
        'view_item'             => __( 'View Video', 'textdomain' ),
        'all_items'             => __( 'All Videos', 'textdomain' ),
        'search_items'          => __( 'Search Videos', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Videos:', 'textdomain' ),
        'not_found'             => __( 'No videos found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No videos found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Video Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Video archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into video', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this video', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter videos list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Videos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Videos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'video' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'menu_icon'          => 'dashicons-format-video',
    );
 
    register_post_type( 'video', $args );
}


function kmvw_admin_init(){
  add_meta_box("youtube_url-meta", "Youtube URL", "youtube_url", "video", "normal", "high");
}

function youtube_url(){
  global $post;

  $custom = get_post_custom($post->ID);
  $youtube_url = isset($custom["youtube_url"][0]) ? $custom["youtube_url"][0] : '';
  $youtube_id = isset($custom["youtube_id"][0]) ? $custom["youtube_id"][0] : '';
  ?>
  <input type='text' class='widefat' id='youtube_url' name='youtube_url' value='<?php echo $youtube_url; ?>' />
  <input type='hidden' id='youtube_id' name='youtube_id' value='<?php echo $youtube_id; ?>' />
  <p id="youtube_id_error" style="color: red; display: none;">Invalid URL</p>
  <script type="text/javascript">
    $('#youtube_url').change(getYoutubeId);
    $('#youtube_url').focus(getYoutubeId);

    function getYoutubeId() {
        $('#youtube_id_error').hide();
        var url = $('#youtube_url').val();
        var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            $('#youtube_id').val(match[2]);
        } else {
            $('#youtube_id_error').attr( "style", "color: red; display: block !important;" );
        }
    }
  </script>
  <?php
}
 
function kmvw_save_details(){
  global $post;

  if(isset($_POST['youtube_url']))
	  update_post_meta($post->ID, 'youtube_url', $_POST['youtube_url']);
  if(isset($_POST['youtube_id']))
      update_post_meta($post->ID, 'youtube_id', $_POST['youtube_id']);
}

add_action( 'init', 'kmvw_video_init' );
add_action('admin_init', 'kmvw_admin_init');
add_action('save_post', 'kmvw_save_details');