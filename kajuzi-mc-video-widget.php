<?php 
/*
Plugin Name: Kajuzi MC Video Widget
Plugin URI: http://kajuzi.com/plugins/mc-video-widget
Description: Adds a video gallery widget to a web page
Version: 0.0.1
Author: Kajuzi
Author URI: http://kajuzi.com/plugins
Text Domain: kmvw
*/

// If this file is called directly, abort. //
if ( ! defined( 'WPINC' ) ) {die;} // end if

// Let's Initialize Everything
if ( file_exists( plugin_dir_path( __FILE__ ) . 'core-init.php' ) ) {
require_once( plugin_dir_path( __FILE__ ) . 'core-init.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/video-post-type.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/gallery-widget.php' );
}